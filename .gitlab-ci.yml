stages:
  - build-orig-tarball
  - build-src
  - build-bin
  - deploy-bin
  - deploy-src

# Build the source tarball exactly once, reproducibly.
build-orig-tarball:
  stage: build-orig-tarball
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:22.04-emulab-builder
  tags:
    - emulab-package-builder
    - amd64
  script:
    - scripts/fix-changelog debian/changelog
    - scripts/setup-source debian/changelog
  artifacts:
    paths:
      - "*.orig.tar.*"

# Build the .dsc source package exactly once per dist.  We don't want to
# have metadata diffs in the dsc that produce conflicting hashes when built
# from the different per-arch builds for the same dist.  So we separate
# source and bin package build, as well as the upload steps.
.build-src-template:
  stage: build-src
  script:
    - curl -o - https://repos.emulab.net/emulab-public.key | apt-key add -
    - echo "deb http://repos.emulab.net/emulab/ubuntu $(. /etc/os-release ; echo $UBUNTU_CODENAME) main" > /etc/apt/sources.list.d/emulab.list
    - apt-get update -y && apt-get install -y libpubsub-dev
    - scripts/fix-changelog debian/changelog
    - scripts/setup-source debian/changelog
    - cp -pRv debian emulab-client-*/
    - cd emulab-client-*
    - dpkg-buildpackage -S -uc -us --changes-option=-sa
    - cd ..
  artifacts:
    paths:
      - "*.changes"
      - "*.dsc"
      - "*.debian.tar.*"
      - "*.orig.tar.*"
      - "*.buildinfo"

ubuntu24-src:
  extends: .build-src-template
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:24.04-emulab-builder
  tags:
    - emulab-package-builder
    - amd64

ubuntu22-src:
  extends: .build-src-template
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:22.04-emulab-builder
  tags:
    - emulab-package-builder
    - amd64

ubuntu20-src:
  extends: .build-src-template
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:20.04-emulab-builder
  tags:
    - emulab-package-builder
    - amd64

ubuntu18-src:
  extends: .build-src-template
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:18.04-emulab-builder
  tags:
    - emulab-package-builder
    - amd64

.build-bin-template:
  stage: build-bin
  variables:
    BUILD_TYPE_ARG: "-b"
  script:
    - curl -o - https://repos.emulab.net/emulab-public.key | apt-key add -
    - echo "deb http://repos.emulab.net/emulab/ubuntu $(. /etc/os-release ; echo $UBUNTU_CODENAME) main" > /etc/apt/sources.list.d/emulab.list
    - apt-get update -y && apt-get install -y libpubsub-dev
    # We don't want the buildinfo and changes files from the build-src
    # stage; just the dsc, debian.tar.xz, and orig tarball.
    # They will confuse the deploy-bin stage, where reprepro attempts to
    # process the incoming dir.
    - rm -fv *.buildinfo *.changes
    - dpkg-source -x emulab-client*.dsc
    - cd emulab-client-*
    - dpkg-buildpackage -uc -us $BUILD_TYPE_ARG
    - cd ..
  artifacts:
    paths:
      - "*.deb"
      - "*.ddeb"
      - "*.changes"
      - "*.dsc"
      - "*.debian.tar.*"
      - "*.orig.tar.*"
      - "*.buildinfo"

ubuntu24-amd64:
  extends: .build-bin-template
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:24.04-emulab-builder
  tags:
    - emulab-package-builder
    - amd64
  dependencies:
    - ubuntu24-src

ubuntu22-amd64:
  extends: .build-bin-template
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:22.04-emulab-builder
  tags:
    - emulab-package-builder
    - amd64
  dependencies:
    - ubuntu22-src

ubuntu20-amd64:
  extends: .build-bin-template
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:20.04-emulab-builder
  tags:
    - emulab-package-builder
    - amd64
  dependencies:
    - ubuntu20-src

ubuntu18-amd64:
  extends: .build-bin-template
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:18.04-emulab-builder
  tags:
    - emulab-package-builder
    - amd64
  dependencies:
    - ubuntu18-src

ubuntu24-aarch64:
  extends: .build-bin-template
  variables:
    BUILD_TYPE_ARG: "-B"
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:24.04-aarch64-emulab-builder
  tags:
    - emulab-package-builder
    - aarch64
  dependencies:
    - ubuntu24-src

ubuntu22-aarch64:
  extends: .build-bin-template
  variables:
    BUILD_TYPE_ARG: "-B"
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:22.04-aarch64-emulab-builder
  tags:
    - emulab-package-builder
    - aarch64
  dependencies:
    - ubuntu22-src

ubuntu20-aarch64:
  extends: .build-bin-template
  variables:
    BUILD_TYPE_ARG: "-B"
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:20.04-aarch64-emulab-builder
  tags:
    - emulab-package-builder
    - aarch64
  dependencies:
    - ubuntu20-src

ubuntu18-aarch64:
  extends: .build-bin-template
  variables:
    BUILD_TYPE_ARG: "-B"
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/ubuntu:18.04-aarch64-emulab-builder
  tags:
    - emulab-package-builder
    - aarch64
  dependencies:
    - ubuntu18-src

.deploy-template:
  image: gitlab.flux.utah.edu:4567/emulab/emulab-client-deb/debian:experimental-emulab-repo-man
  tags:
    - emulab-repo-man
  variables:
    GIT_STRATEGY: none
  before_script:
    - eval $(ssh-agent -s)
    - echo "$REPO_MAN_UPLOAD_PRIVKEY_BASE64" | base64 -d | tr -d '\r' | ssh-add - >/dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - mkdir -p /run/user/0
    - gpgconf --create-socketdir
    - mkdir -p ~/.gnupg
    - echo use-agent >> ~/.gnupg/gpg.conf
    - echo pinentry-mode loopback >> ~/.gnupg/gpg.conf
    - eval $(gpg-agent --daemon --allow-preset-passphrase --allow-loopback-pinentry --default-cache-ttl 21600 --max-cache-ttl 21600)
    - /usr/lib/gnupg2/gpg-preset-passphrase --preset -P "$REPO_MAN_SIGNER_PASSPHRASE" "$REPO_MAN_SIGNER_KEY_FINGERPRINT"
    - /usr/lib/gnupg2/gpg-preset-passphrase --preset -P "$REPO_MAN_SIGNER_PASSPHRASE" "$REPO_MAN_SIGNER_KEYGRIP"
    - echo "$REPO_MAN_SIGNER_KEY_BASE64" | base64 -d | gpg --import --batch --passphrase "$REPO_MAN_SIGNER_PASSPHRASE" -

deploy-bin-testing:
  extends: .deploy-template
  stage: deploy-bin
  script:
    - cp -pv *.deb *.ddeb *.changes *.dsc *.debian.tar.* *.orig.tar.* *.buildinfo /repos/emulab-testing/ubuntu/incoming
    - cd /repos/emulab-testing/ubuntu
    - reprepro -Vb /repos/emulab-testing/ubuntu processincoming default
    - rsync -avz -e 'ssh -o StrictHostKeyChecking=no' /repos/emulab-testing/ubuntu/{db,dists,lists,pool} repoman@ops.emulab.net:/z/linux-package-repos/www/emulab-testing/ubuntu/
  environment:
    name: testing
  only:
    - testing
    - ci-fixes
  dependencies:
    - ubuntu24-amd64
    - ubuntu22-amd64
    - ubuntu20-amd64
    - ubuntu18-amd64
    - ubuntu24-aarch64
    - ubuntu22-aarch64
    - ubuntu20-aarch64
    - ubuntu18-aarch64

deploy-bin-production:
  extends: .deploy-template
  stage: deploy-bin
  script:
    - cp -pv *.deb *.ddeb *.changes *.dsc *.debian.tar.* *.orig.tar.* *.buildinfo /repos/emulab/ubuntu/incoming
    - cd /repos/emulab/ubuntu
    - reprepro -Vb /repos/emulab/ubuntu processincoming default
    - rsync -avz -e 'ssh -o StrictHostKeyChecking=no' /repos/emulab/ubuntu/{db,dists,lists,pool} repoman@ops.emulab.net:/z/linux-package-repos/www/emulab/ubuntu/
  environment:
    name: production
  only:
    - master
  dependencies:
    - ubuntu24-amd64
    - ubuntu22-amd64
    - ubuntu20-amd64
    - ubuntu18-amd64
    - ubuntu24-aarch64
    - ubuntu22-aarch64
    - ubuntu20-aarch64
    - ubuntu18-aarch64

deploy-src-testing:
  extends: .deploy-template
  stage: deploy-src
  script:
    - cp -pv *.changes *.dsc *.debian.tar.* *.orig.tar.* *.buildinfo /repos/emulab-testing/ubuntu/incoming
    - cd /repos/emulab-testing/ubuntu
    - reprepro -Vb /repos/emulab-testing/ubuntu processincoming default
    - rsync -avz -e 'ssh -o StrictHostKeyChecking=no' /repos/emulab-testing/ubuntu/{db,dists,lists,pool} repoman@ops.emulab.net:/z/linux-package-repos/www/emulab-testing/ubuntu/
  environment:
    name: testing
  only:
    - testing
    - ci-fixes
  dependencies:
    - ubuntu24-src
    - ubuntu22-src
    - ubuntu20-src
    - ubuntu18-src

deploy-src-production:
  extends: .deploy-template
  stage: deploy-src
  script:
    - cp -pv *.changes *.dsc *.debian.tar.* *.orig.tar.* *.buildinfo /repos/emulab/ubuntu/incoming
    - cd /repos/emulab/ubuntu
    - reprepro -Vb /repos/emulab/ubuntu processincoming default
    - rsync -avz -e 'ssh -o StrictHostKeyChecking=no' /repos/emulab/ubuntu/{db,dists,lists,pool} repoman@ops.emulab.net:/z/linux-package-repos/www/emulab/ubuntu/
  environment:
    name: production
  only:
    - master
  dependencies:
    - ubuntu24-src
    - ubuntu22-src
    - ubuntu20-src
    - ubuntu18-src
